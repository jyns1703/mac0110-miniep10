using Test

function test()
	@test max_sum_submatrix([1 1 1 ; 1 1 1 ; 1 1 1]) == [[1 1 1 ; 1 1 1 ; 1 1 1]]
	@test max_sum_submatrix([0 -1 ; -2 -3]) == [zeros(Int64,1,1)]
	@test max_sum_submatrix([-1 1 1 ; 1 -1 1 ; 1 1 -2]) == [[1 1; -1 1],[1 -1; 1 1],[-1 1 1; 1 -1 1; 1 1 -2]]
	@test max_sum_submatrix([9 -1 0; -2 -3 0;6 0 0]) == [fill(9,1,1),[9 -1 0;-2 -3 0;6 0 0]]
	println("fim dos testes")
end

function soma(m)
	s = 0
	for i in 1 : size(m,1)
		for j in 1 : size(m,2)
			s = s + m[i,j]
		end
	end
	return s
end

function subMatriz(m,i,j,k)
	u = zeros(Int,k,k)
	for a in 1 : k 
		for b in 1 : k 
			u[a,b] = m[i+a-1,j+b-1]
		end
	end
	return u
end

function max_sum_submatrix(m)
	s = 0
	v = []
	n = size(m,1)
	for k in 1 : n
		for i in 1 : n
			for j in 1 : n
				if i + k - 1 <= n && j + k - 1 <= n
					if soma(subMatriz(m,i,j,k)) > s
						s = soma(subMatriz(m,i,j,k))
						v = []
						push!(v,subMatriz(m,i,j,k))
					elseif soma(subMatriz(m,i,j,k)) == s
						push!(v,subMatriz(m,i,j,k))
					end
				end
			end
		end
	end
	return v
end

